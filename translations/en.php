<?php
/**
 * Layouts plugin for Craft CMS
 *
 * Layouts Translation
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

return array(
    'Select which layouts will be available to the user. Update handle, title, description and move them in the display order.' => 'Select which layouts will be available to the user. Update handle, title, description and move them in the display order.',
    'Layouts' => 'Layouts',
    'Collapse' => 'Collapse',
    'Expand'   => 'Expand'
);
