<?php
namespace Craft;

class Layouts_LayoutsController extends BaseController
{
    protected $allowAnonymous = ['actionDefaultLayouts'];

    public function actionLoadElementSelectField ()
    {
        $this->requireAjaxRequest();
        $variables = array();
        $variables['sources'] = craft()->request->getPost('sources');
        $variables['entryTypes'] = craft()->request->getPost('entrytypes');
        $variables['limit'] = craft()->request->getPost('limit');
        $variables['name'] = craft()->request->getPost('limit');
        $variables['id'] = craft()->request->getPost('id');

        $this->renderTemplate('layouts/fields/_elementSelect', $variables, false);
    }

}
