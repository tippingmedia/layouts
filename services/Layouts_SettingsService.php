<?php
namespace Craft;

class Layouts_SettingsService extends BaseApplicationComponent
{
    public function defaultLayouts() {
        return LayoutsHelper::defaultLayouts();
    }

    public function availableLayouts() {
        $plugin  = craft()->plugins->getPlugin('layouts');
        $settings = $plugin->getSettings();
        return $settings['layouts'];
    }
}
