<?php
namespace Craft;

class LayoutsVariable
{

    /**
     * All available layouts with default names.
     */
    function defaultLayouts()
	{
        return craft()->layouts_settings->defaultLayouts();
    }

    /**
     * Selected Layouts from the settings.
     */
    function selectedLayouts()
    {
        return craft()->layouts_settings->availableLayouts();
    }

    /**
     * Selected Layouts from the settings.
     */
    function layouts()
    {
        return craft()->layouts_settings->availableLayouts();
    }
}
