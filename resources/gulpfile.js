/*
 * Gulp install plugins
 * @package.json
 */
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const $ = gulpLoadPlugins();
const cleanCSS = require('gulp-clean-css');
const sass = require('gulp-ruby-sass');
const babel = require('gulp-babel');

require("babel-core").transform("code", {
  presets: ["es2015"]
});


/*
 * Paths for watch,process & bower
 */
var paths = {
    watch: {
        scripts: ['js/**/*.js'],
        styles:['css/sass/**/*.scss'],
    },
    process: {
        scripts:[
            'js/fields/Layouts_LayoutsFieldType.js',
        ],
        sass:['css/sass/**/*']
    }
};




/*
 * jsHint
 */
gulp.task('jshint', function () {
    console.log('–:::– JSHINT –:::–');
    return gulp.src(['js/fields/**/*.js'])
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
});







/*
 * Contact script files to master.js
 */
gulp.task('scripts', function(){
    console.log('–:::– SCRIPTS –:::–');
    return gulp.src(paths.process.scripts)
        .pipe($.changed('js/fields/**/*.js'))
        .pipe($.concat('layouts.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('js/dist/'));
});




/*
 * Minify master.js => master.min.js
 */
gulp.task('compress',['scripts'],function(){
    return gulp.src('js/dist/layouts.js')
        .pipe($.jsmin())
        .pipe($.rename({suffix: '.min'}))
        .pipe(gulp.dest('js/dist/'));
});



/*
 * Notification when script process is done.
 */
gulp.task('notify',['compress'],function(){
    return gulp.src("notify.ext")
     .pipe($.notify({
        "title": "Layouts Craft Plugin",
        //"subtitle": "Project web site",
        "message": "Script processing successful!",
        "sound": "Morse", // case sensitive
        "onLast": true,
        "wait": true
    }));
});


/*
 * Notification when script process is done.
 */
gulp.task('notify-css',['styles'],function(){
    return gulp.src("notify.ext")
     .pipe($.notify({
        "title": "Layouts Craft Plugin",
        //"subtitle": "Project web site",
        "message": "CSS processing successful!",
        "sound": "Morse", // case sensitive
        "onLast": true,
        "wait": true
    }));
});


/*
 * sass build
 */
gulp.task('sass-app',function(){
    console.log('–:::SASS:::–');
     return sass('css/sass/')
        .on('error', function (err) {
            console.error('Error!', err.message);
     })
        .pipe(gulp.dest('css/dist/'));
});



/*
 * Minify & autoprefix styles
 */
gulp.task('styles',['sass-app'],function(){
    console.log('–:::STYLES:::–');
    return gulp.src('css/dist/**/*')
        .pipe($.concat('layouts.css'))
        .pipe(cleanCSS(function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest('css/dist/'));
});



/*
 * Watch 'default'
 */
gulp.task('default', function() {
    gulp.watch(paths.watch.scripts, ['scripts','compress','notify']);
    gulp.watch(paths.watch.styles, ['sass-app','styles','notify-css']);
    //gulp.watch(paths.images, ['images']);
});
