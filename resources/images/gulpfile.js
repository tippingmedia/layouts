const autoprefixer = require( 'autoprefixer' )
const browserSync = require( 'browser-sync' ).create()
const cheerio = require( 'gulp-cheerio' )
const gulp = require( 'gulp' )
const imagemin = require( 'gulp-imagemin' )
const include = require( 'gulp-include' )
const newer = require( 'gulp-newer' )
const perfectionist = require( 'perfectionist' )
const plumber = require( 'gulp-plumber' )
const postcss = require( 'gulp-postcss' )
const rename = require( 'gulp-rename' )
const svgstore = require( 'gulp-svgstore' )
const uglify = require( 'gulp-uglify' )

const paths = {
  icons: {
    src: 'layouts/**/*',
    dest: 'icons',
    watch: 'layouts/**/*'
  },
  img: {
    src: 'assets/src/img/**/*',
    dest: 'assets/dist/img',
    watch: 'assets/src/img/**/*'
  },
}



gulp.task( 'icons', function() {
  return gulp.src( paths.icons.src )
    .pipe( plumber() )
    .pipe( imagemin() )
    .pipe( rename( function( path ) {
      var name = path.dirname.split( path.sep )
      name.push( path.basename )
      path.basename = name.join( '-' )
    } ) )
    .pipe( rename( { prefix: 'icon-' } ) )
    .pipe( cheerio( {
      run: function( $ ) {
        //$( '[fill]' ).removeAttr( 'fill' )
      },
      parserOptions: { xmlMode: true },
    } ) )
    .pipe( svgstore( { inlineSvg: true } ) )
    .pipe( cheerio( function( $ ) {
      $( 'svg' ).attr( 'display', 'none' )
    } ) )
    .pipe( gulp.dest( paths.icons.dest ) )
    //.pipe( browserSync.stream() )
} )

gulp.task( 'img', function() {
  return gulp.src( paths.img.src )
    .pipe( plumber() )
    .pipe( newer( paths.img.dest ) )
    .pipe( imagemin() )
    .pipe( gulp.dest( paths.img.dest ) )
    //.pipe( browserSync.stream() )
} )



gulp.task( 'default', [ 'watch' ] )


// gulp.task( 'bs', [ 'watch' ], function() {
//   browserSync.init( {
//     proxy: 'localhost/redeemer',
//     notify: false,
//   } )
//
//   gulp.watch( '**/*.php' ).on( 'change',  browserSync.reload )
// } )
