/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_LayoutsSettings JS
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

class LayoutsSettings {
    constructor (element) {
        this.field = document.querySelector(`#${element}`);
        this.layouts = this.field.querySelectorAll('.layout--element');


        for (const layout of this.layouts) {
            layout.addEventListener( 'click', this.select );
            const layoutDataInput = $(layout).find('input.layout--data');
            const hudContainer = $(layout).find('.layout--sources-hud');
            layout.hud = new Garnish.HUD( $(layout).find('.edit'), hudContainer, {
                onShow: $.proxy(this.showHud),
                onHide: $.proxy(this.hideHud)
            });

            layout.hud.layoutDataInput = layoutDataInput;

            if (layout.hud.showing) {
                layout.hud.hide();
            }

            const sourceCheckboxes = layout.hud.$mainContainer.find('.layout--source-checkbox');

            for (const checkbox of sourceCheckboxes) {
                checkbox.addEventListener('click', function (evt) {
                    const value = this.value;
                    const entryTypes = layout.hud.$mainContainer.find(`input[data-section-id="${value}"]`);
                    if(this.checked) {
                        for(const type of entryTypes) {
                            type.disabled = false;
                            type.checked = true;
                        }
                    }else{
                        for(const type of entryTypes) {
                            type.disabled = true;
                            type.checked = false;
                        }
                    }
                });
            }
        }

        if (!this.dragSort) {
            this.dragSort = new Garnish.DragSort(this.layouts, {
    			handle: '.move',
    			axis: 'x,y'
    		});
        }

    }

    select (evt) {
        const nodes = ["INPUT","LABEL","TEXTAREA"];
        const nodeName = evt.target.nodeName;
        if (nodes.includes(nodeName)) {
            return false;
        }
        const _this = this;
        const layout = evt.currentTarget;
        const inputs = layout.querySelectorAll('input');
        const hud = layout.hud;

        if (nodeName === "A" && evt.target.classList.contains('edit')) {
            evt.preventDefault();
            hud.show();
            return false;
        }


        if (layout.classList.contains('selected')) {
            layout.classList.remove('selected');
            for (const input of inputs) {
                input.disabled = true;
            }
            hud.$mainContainer.find('input').prop('disabled',true);
        }else{
            layout.classList.add('selected');
            for (const input of inputs) {
                input.disabled = false;
            }
            hud.$mainContainer.find('input').prop('disabled',false);

            const form = hud.$body;
            const data = form.serializeJSON();
            const layouts = nestedObj("layouts", data);

            hud.layoutDataInput.val(JSON.stringify(layouts));
        }
    }

    showHud (hud) {
        //hud.$mainContainer.find('input')
    }

    hideHud (hudObj) {
        const hud = hudObj.target;
        const form = hud.$body;
        const data = form.serializeJSON();
        const layouts = nestedObj("layouts", data);

        hud.layoutDataInput.val(JSON.stringify(layouts));

    }
}

// Layouts Field

class LayoutsField {

    constructor (element, parameters) {
        this.namespace = parameters.namespace;
        this.name = parameters.name;
        this.nameId = parameters.nameId;
        this.id = parameters.id;
        this.prefix = parameters.prefix;

        this.field = document.querySelector(`#${this.namespace}-field`);
        this.fieldSelected = this.field.querySelector(`#${this.namespace}-layout`);
        this.layouts = document.querySelectorAll(element);
        this.actions = this.field.querySelector('.actions');

        for (const layout of this.layouts) {
            layout.params = parameters;
            layout.loadSelectField = this.loadSelectField;
            layout.addEventListener('click', this.select );
        }

        this.actions.addEventListener('click', this.toggleClass );

        this.fieldSelected.addEventListener('click', this.removeLayout);
    }

    select (evt) {
        const _this = this;
        const layout = evt.currentTarget;
        const layoutMeta = JSON.parse(layout.dataset.value);
        const field = $(`#${layout.params.namespace}-layout`);
        const input = field.find('.layout__input-handle');
        const iconWrap = field.find('.layout__icon');
        const iconWrapSM = field.prev().find('.layout__icon-small');
        const descWrap = field.find('.layout__description span');
        const baseName = layout.params.nameId;
        const deleteBtn = ` <a data-icon="remove" class="layout__remove"></a>`;

        if (field.hasClass('hidden')) {
            field.removeClass('hidden');
        }


        input.val(layoutMeta.handle);
        descWrap.html(layoutMeta.description);
        iconWrap.html(icon(100,layoutMeta.icon, baseName));
        iconWrapSM.html(icon(24,layoutMeta.icon, baseName));

        function icon (size, name, baseName = "fields") {
            return `<svg class='icn icon-${size}'><use xlink:href='#${baseName}-icon-${name}'></use></svg>`;
        }

        this.loadSelectField(layoutMeta,layout.params.namespace,input.attr('name').replace('[handle]',''),field);

    }

    toggleClass (evt) {
        const action = evt.target;
        const layoutsElements = action.parentNode.parentNode.parentNode

        if ( action.classList.contains('collapse') ) {
            layoutsElements.classList.add('collapsed');
        } else if( action.classList.contains('expand') ) {
            layoutsElements.classList.remove('collapsed');
        }
    }

    removeLayout (evt) {
        evt.preventDefault();
        const field = $(evt.currentTarget);
        const input = field.find('input[type=hidden]');
        const descWrap = field.find('.layout__description span');
        const iconWrap = field.find('.layout__icon');
        const iconWrapSM = field.prev().find('.layout__icon-small');


        if (evt.target.nodeName === "A" && evt.target.classList.contains("layout__remove")) {

            field.addClass('hidden');

            input.attr("value","");
            descWrap.html("");
            iconWrap.html("");
            iconWrapSM.html("");
        }else{
            return false;
        }
    }

    loadSelectField (settings, namespace, name, field) {
        const selectField = field.find('.layout--element-selector');

        const data = {
            'name': `${name}[entries]`,
            'id': `${namespace}-element-select`,
            'sources': settings.sources,
            'entrytypes': settings.entrytypes,
            'limit': settings.limit
        }

        Craft.postActionRequest('layouts/layouts/loadElementSelectField', data, $.proxy(function(response, textStatus)
        {
            if (textStatus == 'success') {
                selectField.html(response);
            }

        }, this));
    }
}

const nestedObj = function (key, data) {
    for(const prop in data) {
        if(data.hasOwnProperty(prop)) {
            if (key === prop) {
                return data[prop];
            } else {
                return nestedObj(key, data[prop]);
            }
        }
    }
    return null;
}
