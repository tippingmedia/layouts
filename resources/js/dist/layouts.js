'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_LayoutsSettings JS
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

var LayoutsSettings = function () {
    function LayoutsSettings(element) {
        var _this2 = this;

        _classCallCheck(this, LayoutsSettings);

        this.field = document.querySelector('#' + element);
        this.layouts = this.field.querySelectorAll('.layout--element');

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            var _loop = function _loop() {
                var layout = _step.value;

                layout.addEventListener('click', _this2.select);
                var layoutDataInput = $(layout).find('input.layout--data');
                var hudContainer = $(layout).find('.layout--sources-hud');
                layout.hud = new Garnish.HUD($(layout).find('.edit'), hudContainer, {
                    onShow: $.proxy(_this2.showHud),
                    onHide: $.proxy(_this2.hideHud)
                });

                layout.hud.layoutDataInput = layoutDataInput;

                if (layout.hud.showing) {
                    layout.hud.hide();
                }

                var sourceCheckboxes = layout.hud.$mainContainer.find('.layout--source-checkbox');

                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = sourceCheckboxes[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var checkbox = _step2.value;

                        checkbox.addEventListener('click', function (evt) {
                            var value = this.value;
                            var entryTypes = layout.hud.$mainContainer.find('input[data-section-id="' + value + '"]');
                            if (this.checked) {
                                var _iteratorNormalCompletion3 = true;
                                var _didIteratorError3 = false;
                                var _iteratorError3 = undefined;

                                try {
                                    for (var _iterator3 = entryTypes[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                        var type = _step3.value;

                                        type.disabled = false;
                                        type.checked = true;
                                    }
                                } catch (err) {
                                    _didIteratorError3 = true;
                                    _iteratorError3 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                            _iterator3.return();
                                        }
                                    } finally {
                                        if (_didIteratorError3) {
                                            throw _iteratorError3;
                                        }
                                    }
                                }
                            } else {
                                var _iteratorNormalCompletion4 = true;
                                var _didIteratorError4 = false;
                                var _iteratorError4 = undefined;

                                try {
                                    for (var _iterator4 = entryTypes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                                        var _type = _step4.value;

                                        _type.disabled = true;
                                        _type.checked = false;
                                    }
                                } catch (err) {
                                    _didIteratorError4 = true;
                                    _iteratorError4 = err;
                                } finally {
                                    try {
                                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                            _iterator4.return();
                                        }
                                    } finally {
                                        if (_didIteratorError4) {
                                            throw _iteratorError4;
                                        }
                                    }
                                }
                            }
                        });
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            };

            for (var _iterator = this.layouts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                _loop();
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        if (!this.dragSort) {
            this.dragSort = new Garnish.DragSort(this.layouts, {
                handle: '.move',
                axis: 'x,y'
            });
        }
    }

    _createClass(LayoutsSettings, [{
        key: 'select',
        value: function select(evt) {
            var nodes = ["INPUT", "LABEL", "TEXTAREA"];
            var nodeName = evt.target.nodeName;
            if (nodes.includes(nodeName)) {
                return false;
            }
            var _this = this;
            var layout = evt.currentTarget;
            var inputs = layout.querySelectorAll('input');
            var hud = layout.hud;

            if (nodeName === "A" && evt.target.classList.contains('edit')) {
                evt.preventDefault();
                hud.show();
                return false;
            }

            if (layout.classList.contains('selected')) {
                layout.classList.remove('selected');
                var _iteratorNormalCompletion5 = true;
                var _didIteratorError5 = false;
                var _iteratorError5 = undefined;

                try {
                    for (var _iterator5 = inputs[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                        var input = _step5.value;

                        input.disabled = true;
                    }
                } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }
                    } finally {
                        if (_didIteratorError5) {
                            throw _iteratorError5;
                        }
                    }
                }

                hud.$mainContainer.find('input').prop('disabled', true);
            } else {
                layout.classList.add('selected');
                var _iteratorNormalCompletion6 = true;
                var _didIteratorError6 = false;
                var _iteratorError6 = undefined;

                try {
                    for (var _iterator6 = inputs[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                        var _input = _step6.value;

                        _input.disabled = false;
                    }
                } catch (err) {
                    _didIteratorError6 = true;
                    _iteratorError6 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion6 && _iterator6.return) {
                            _iterator6.return();
                        }
                    } finally {
                        if (_didIteratorError6) {
                            throw _iteratorError6;
                        }
                    }
                }

                hud.$mainContainer.find('input').prop('disabled', false);

                var form = hud.$body;
                var data = form.serializeJSON();
                var layouts = nestedObj("layouts", data);

                hud.layoutDataInput.val(JSON.stringify(layouts));
            }
        }
    }, {
        key: 'showHud',
        value: function showHud(hud) {
            //hud.$mainContainer.find('input')
        }
    }, {
        key: 'hideHud',
        value: function hideHud(hudObj) {
            var hud = hudObj.target;
            var form = hud.$body;
            var data = form.serializeJSON();
            var layouts = nestedObj("layouts", data);

            hud.layoutDataInput.val(JSON.stringify(layouts));
        }
    }]);

    return LayoutsSettings;
}();

// Layouts Field

var LayoutsField = function () {
    function LayoutsField(element, parameters) {
        _classCallCheck(this, LayoutsField);

        this.namespace = parameters.namespace;
        this.name = parameters.name;
        this.nameId = parameters.nameId;
        this.id = parameters.id;
        this.prefix = parameters.prefix;

        this.field = document.querySelector('#' + this.namespace + '-field');
        this.fieldSelected = this.field.querySelector('#' + this.namespace + '-layout');
        this.layouts = document.querySelectorAll(element);
        this.actions = this.field.querySelector('.actions');

        var _iteratorNormalCompletion7 = true;
        var _didIteratorError7 = false;
        var _iteratorError7 = undefined;

        try {
            for (var _iterator7 = this.layouts[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                var _layout = _step7.value;

                _layout.params = parameters;
                _layout.loadSelectField = this.loadSelectField;
                _layout.addEventListener('click', this.select);
            }
        } catch (err) {
            _didIteratorError7 = true;
            _iteratorError7 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion7 && _iterator7.return) {
                    _iterator7.return();
                }
            } finally {
                if (_didIteratorError7) {
                    throw _iteratorError7;
                }
            }
        }

        this.actions.addEventListener('click', this.toggleClass);

        this.fieldSelected.addEventListener('click', this.removeLayout);
    }

    _createClass(LayoutsField, [{
        key: 'select',
        value: function select(evt) {
            var _this = this;
            var layout = evt.currentTarget;
            var layoutMeta = JSON.parse(layout.dataset.value);
            var field = $('#' + layout.params.namespace + '-layout');
            var input = field.find('.layout__input-handle');
            var iconWrap = field.find('.layout__icon');
            var iconWrapSM = field.prev().find('.layout__icon-small');
            var descWrap = field.find('.layout__description span');
            var baseName = layout.params.nameId;
            var deleteBtn = ' <a data-icon="remove" class="layout__remove"></a>';

            if (field.hasClass('hidden')) {
                field.removeClass('hidden');
            }

            input.val(layoutMeta.handle);
            descWrap.html(layoutMeta.description);
            iconWrap.html(icon(100, layoutMeta.icon, baseName));
            iconWrapSM.html(icon(24, layoutMeta.icon, baseName));

            function icon(size, name) {
                var baseName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "fields";

                return '<svg class=\'icn icon-' + size + '\'><use xlink:href=\'#' + baseName + '-icon-' + name + '\'></use></svg>';
            }

            this.loadSelectField(layoutMeta, layout.params.namespace, input.attr('name').replace('[handle]', ''), field);
        }
    }, {
        key: 'toggleClass',
        value: function toggleClass(evt) {
            var action = evt.target;
            var layoutsElements = action.parentNode.parentNode.parentNode;

            if (action.classList.contains('collapse')) {
                layoutsElements.classList.add('collapsed');
            } else if (action.classList.contains('expand')) {
                layoutsElements.classList.remove('collapsed');
            }
        }
    }, {
        key: 'removeLayout',
        value: function removeLayout(evt) {
            evt.preventDefault();
            var field = $(evt.currentTarget);
            var input = field.find('input[type=hidden]');
            var descWrap = field.find('.layout__description span');
            var iconWrap = field.find('.layout__icon');
            var iconWrapSM = field.prev().find('.layout__icon-small');

            if (evt.target.nodeName === "A" && evt.target.classList.contains("layout__remove")) {

                field.addClass('hidden');

                input.attr("value", "");
                descWrap.html("");
                iconWrap.html("");
                iconWrapSM.html("");
            } else {
                return false;
            }
        }
    }, {
        key: 'loadSelectField',
        value: function loadSelectField(settings, namespace, name, field) {
            var selectField = field.find('.layout--element-selector');

            var data = {
                'name': name + '[entries]',
                'id': namespace + '-element-select',
                'sources': settings.sources,
                'entrytypes': settings.entrytypes,
                'limit': settings.limit
            };

            Craft.postActionRequest('layouts/layouts/loadElementSelectField', data, $.proxy(function (response, textStatus) {
                if (textStatus == 'success') {
                    selectField.html(response);
                }
            }, this));
        }
    }]);

    return LayoutsField;
}();

var nestedObj = function nestedObj(key, data) {
    for (var prop in data) {
        if (data.hasOwnProperty(prop)) {
            if (key === prop) {
                return data[prop];
            } else {
                return nestedObj(key, data[prop]);
            }
        }
    }
    return null;
};