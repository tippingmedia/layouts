# Layouts plugin for Craft CMS

Layouts is a plugin field type to help your content editors understand what type of layout their content will output.

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install Layouts, follow these steps:

1. Download & unzip the file and place the `layouts` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /layouts`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `layouts` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

Layouts works on Craft 2.4.x and Craft 2.5.x.

## Layouts Overview

[![Layouts Demo Video](https://img.youtube.com/vi/l55ldG6mP3I/0.jpg)](https://www.youtube.com/watch?v=l55ldG6mP3I)

## Layouts Changelog

### 1.0.0 -- 2016.12.16

* Initial release

Brought to you by [Tipping Media LLC](http://tippingmedia.com)
