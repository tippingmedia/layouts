<?php
/**
 * Layouts plugin for Craft CMS
 *
 * Layouts_Layouts Model
 *
 *
 * @author    Tipping Media LLC
 * @copyright Copyright (c) 2016 Tipping Media LLC
 * @link      http://tippingmedia.com
 * @package   Layouts
 * @since     1.0.0
 */

namespace Craft;

class Layouts_LayoutFieldModel extends BaseModel
{

    public function __toString()
	{
			return $this->id . '-' . $this->handle;
	}

    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'id'            => array(AttributeType::Number),
            'handle'        => array(AttributeType::String),
            'description'   => array(AttributeType::String),
            'icon'          => array(AttributeType::String),
            'entries'       => array(AttributeType::Mixed),
            'sources'       => array(AttributeType::Mixed),
            'entryTypes'    => array(AttributeType::Mixed),
            'limit'         => array(AttributeType::Number)
        ));
    }

    /**
	 * @inheritDoc BaseModel::getAttribute()
	 *
	 * @param string $name
	 * @param bool   $flattenValue
	 *
	 * Updates location variable to return Venti_Location element criteria model else return normal.
	 * @return mixed
	 */
	public function getAttribute($name, $flattenValue = false)
	{
        if($name === 'entries' && is_array(parent::getAttribute('entries')))
		{
            $criteria = craft()->elements->getCriteria("Entry");
	        $criteria->id = parent::getAttribute('entries');
            $criteria->fixedOrder = true;
	        return $criteria;
        }

        return parent::getAttribute($name, $flattenValue);
    }

}
